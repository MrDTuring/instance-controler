## Basic Usage ##
1. Run "IC.exe"
2. Use the browse button to and then navigate to the exe that you wish to use - click open.
3. Configure your options (a full explanation of all options can be found below).
4. Once you have filled in all of your options, click "Apply".
5. Once you have read through the message and clicked "Yes", this will then generate a ic-config.ini file in the same directory.
6. When you run IC.exe from now on, it will launch the application you pointed it to, with the options you configured.

## Making changes to an existing configuration ##
If you don't need the GUI, making changes to the ini directly is fine.

1. Navigate to the directory "IC.exe" resides within.
2. Find the file "ic-config.ini" and open it in your favorite text editor.
3. Look for the option "DisplayOptions" and change to "true"
4. Save this file
5. Run IC.exe again, you will now be able to make changes to configuration and re-apply it.

## Settings Explanation ##
1. **Executable Path -** This should be the direct path to the executable, including its file name and extension.
2. **Allow Only One Instance -** Tell IC to perform a check, to see if the process of the targeted application is already running. If it is, it will prevent another from opening.
3. **Prompt Override -** Warn the user that another instance is already running, but will allow the user to override this warning and launch a new one.
4. **Display Message -** Display a message prior to executing the application, this won't allow the user to continue until they have acknowledged the message (this is as simple as clicking 'OK').
5. **Message -** Becomes active when "Display Message" is enabled, this is where you type the message you wish to display pre-execution.
6. **Log Execution -** Logs any attempt to open the software (successful or not), IC will put a new entry complete with date and time, as well as any information such as whether an override was used.
7. **Save Log to -** This is the directory of the log file you wish to write to, as well as the log name on the end.
8. **Launch with arguments -** Launch the targeted program with the command line arguments entered into the box to the right.


If you need any further assistance or you're having problems, contact me at **daniel@dkoz.co.uk**


## License ##
This project is published under the [Create Commons Share and Share Alike License](http://creativecommons.org/licenses/by-sa/4.0/).